<?php 

/**
 * @file
 * Forms API specific functions for the videopublishing module.
 */

/**
 * Defines the upload (/ insert video) form
 * 
 * This form is hooked into the node/add and node/<nid>/edit forms
 */
function videopublishing_upload_form($form_state, $video = NULL) {

  $form['#attributes']['enctype'] = 'multipart/form-data';
  
  $form['videopublishing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Video Publishing settings'),
    '#weight' => variable_get('videopublishing_field_weight', 10),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  
  $form['videopublishing']['input_type'] = array(
    '#type' => 'select',
    '#title' => t('Select video input type'),
    '#default_value' => 'video_id',
    '#options' => array(
      'video_id' => 'Video ID',
      'upload' => 'Video Upload',
      ),
    '#description' => t('Select the desired method for attaching the video'),
  );
  
  if ($video) {
    $default_vp_player_id = $video['player_id'];
  }
  else {
    $default_vp_player_id = variable_get('videopublishing_default_player', NULL);
  }
  $form['videopublishing']['player_id'] = array(
    '#type' => 'select',
    '#title' => t('Select player'),
    '#default_value' => $default_vp_player_id,
    '#options' => videopublishing_settings_form_get_player_options(),
    '#description' => t('You can create your own custom players through the Video Publishing GUI. See <a href="http://admin.videopublishing.com/players/" title="VideoPublishing Players" rel="nofollow">http://admin.videopublishing.com/players/</a>'),
  );
  
  $video_options = videopublishing_get_all_video_options();
  $video_id_widget = variable_get('videopublishing_video_id_widget', 'select');
  if ($video) {
    $default_vp_video_id = $video['video_id'];
    switch ($video_id_widget) {
      case 'select':
        $default_vp_video_id_value = $default_vp_video_id;
        break;
      case 'autocomplete':
        $default_vp_video_id_value = $video_options[$default_vp_video_id] .' [video_id:'. $default_vp_video_id .']';
        break;
    }
  }
  else {
    $default_vp_video_id = '';
  }
  if ($form_state['values']['videopublishing']['video_id']) {
    $default_vp_video_id = $form_state['values']['videopublishing']['video_id'];
  }  

  switch ($video_id_widget) {
    case 'select':
      $form['videopublishing']['video_id'] = array(
        '#type' => 'select',
        '#title' => t('Video ID'),
        '#default_value' => $default_vp_video_id_value,
        '#description' => t('The video ID provided by VideoPublishing.com.'),
        '#options' => $video_options,
        '#ahah' => array(
          'path' => 'videopublishing/get/publish-id-options',
          'wrapper' => 'vp-publish-id-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
          'event' => 'change',
        ),
      );
      break;

    case 'autocomplete':
      $form['videopublishing']['video_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Video ID'),
        '#default_value' => $default_vp_video_id_value,
        '#description' => t('The video ID provided by VideoPublishing.com.'),
        '#autocomplete_path' => 'videopublishing/autocomplete/videos',
        '#ahah' => array(
          'path' => 'videopublishing/get/publish-id-options',
          'wrapper' => 'vp-publish-id-wrapper',
          'method' => 'replace',
          'effect' => 'fade',
          'event' => 'change',
        ),
      );
      break;
  }
  
  // Add a wrapper for the publish id.
  $form['videopublishing']['publish_id_wrapper'] = array(
    '#tree' => FALSE,
    '#prefix' => '<div class="clear-block" id="vp-publish-id-wrapper">',
    '#suffix' => '</div>',
  );
  
  if ($video) {
    $default_vp_publish_id = $video['publish_id'];
  }
  else {
    $default_vp_publish_id = '';
  }
  if ($form_state['values']['videopublishing']['input_type']) {
    $default_vp_publish_id = $form_state['values']['videopublishing']['publish_id'];
  }
  $form['videopublishing']['publish_id_wrapper']['publish_id'] = array(
    '#type' => 'select',
    '#title' => t('Publish ID'),
    '#default_value' => $default_vp_publish_id,
    '#options' => videopublishing_get_associated_publish_id_options($default_vp_video_id),
    '#description' => t('The publish ID provided by VideoPublishing.com.'),
//    '#element_validate' => array('videopublishing_publish_id_ahah_form_validate'),
  );
  
  $form['videopublishing']['video_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Video name'),
    '#size' => 40,
    '#description' => t('This field is required by Video Publishing and will be stored on the Video Publishing server.'),
  );
  
  $form['videopublishing']['format_id'] = array(
    '#type' => 'select',
    '#title' => t('Video Format'),
    '#options' => videopublishing_get_format_options(),
  );
  
  $form['videopublishing']['vp_video_upload_field'] = array(
    '#type' => 'file',
    '#title' => t('Attach a new video'),
    '#size' => 40,
    '#description' => t('Select a valid video file.'),
  );
    
  /**/
  
  
  
  return $form;
}


function videopublishing_autocomplete_videos($string = '') {
  $matches = array();
  
  $vids = videopublishing_get_all_video_options(FALSE);
  if ($vids) {
    foreach ($vids as $video_id => $video_title) {
      if (preg_match("/{$string}/i", $video_title)) {
        $matches[$video_title .' [video_id:'. $video_id .']'] = $video_title;
      }
    }
  }
  
  drupal_json($matches);
}


function videopublishing_publish_id_ahah_form() {

  module_load_include('inc', 'node', 'node.pages');
  
  switch (variable_get('videopublishing_video_id_widget', 'select')) {
    case 'select':
      $video_id = $_POST['videopublishing']['video_id'];
      break;
    
    case 'autocomplete':
      preg_match('/^(?:\s*|(.*) )?\[\s*video_id\s*:\s*([\da-f]*)\s*\]$/', $_POST['videopublishing']['video_id'], $matches);
      $video_id = $matches[2];
      break;
  }
  
  // Build the new form.
  $form_state = array('submitted' => FALSE, 'storage' => NULL);
  $form_build_id = $_POST['form_build_id'];
  
  // Add the new element to the stored form.
  // Retreive the cached form, add the element, and resave.
  $form = form_get_cache($form_build_id, $form_state);
  
  // -- Build our new form element.
  $form_element = $form['videopublishing']['publish_id_wrapper']['publish_id'];
  $form_element['#options'] = videopublishing_get_associated_publish_id_options($video_id);
  
  $form['videopublishing']['publish_id_wrapper']['publish_id'] = $form_element;
  form_set_cache($form_build_id, $form, $form_state);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );
  
  // Rebuild the form.
  $form = form_builder('videopublishing_upload_form', $form, $form_state);
  
  // Render the new output.
  $subform = $form['videopublishing']['publish_id_wrapper']['publish_id'];
  $output = theme('status_messages') . drupal_render($subform);
  
  drupal_json(array('status' => TRUE, 'data' => $output));
}


/**
 * Defines a custom validation callback 
 * It checks if a valid video_id was provided
 * 
 * This callback is hooked into the node/add and node/<nid>/edit forms
 */
function videopublishing_upload_form_validate($form, &$form_state) {
  // the publish_id is added in $form_state['values'], not
  // $form_state['values']['videopublishing'] because of
  // 'publish_id_wrapper' #tree => FALSE
  // we'll add it to $form_state['values']['videopublishing'] for consistency
  if (isset($form_state['values']['publish_id'])) {
    $form_state['values']['videopublishing']['publish_id'] = $form_state['values']['publish_id'];
  }
  
  switch ($form_state['values']['videopublishing']['input_type']) {
    
    case 'video_id':
      
      switch (variable_get('videopublishing_video_id_widget', 'select')) {
        case 'select':
          $video_id = $form_state['values']['videopublishing']['video_id'];
          break;
        case 'autocomplete':
          preg_match('/^(?:\s*|(.*) )?\[\s*video_id\s*:\s*([\da-f]*)\s*\]$/', $form_state['values']['videopublishing']['video_id'], $matches);
          $video_id = $matches[2];
          break;
      }
      
      if ($video_id) {
        $video_info = videopublishing_get_video_info($video_id);
        if (!$video_info) {
          form_set_error('videopublishing][video_id', t('Invalid Video ID'));
        }
        $form_state['values']['videopublishing']['video_id'] = $video_id;
      }
      else {
        form_set_error('videopublishing][video_id', t('Invalid Video ID'));
      }
      
      if (!$form_state['values']['videopublishing']['publish_id']) {
        // note the tree => FALSE
        form_set_error('publish_id', t('Invalid Publish ID'));
      }
      
      break;
    
    case 'upload':
      
      $tmp_file = $_FILES['files']['tmp_name']['videopublishing'];
      $video_name = check_plain($form_state['values']['videopublishing']['video_name']);
      if (!$tmp_file) {
        form_set_error('videopublishing][vp_video_upload_field', t('You must select a valid video file.'));
      }/**/
      if (!$video_name) {
        form_set_error('videopublishing][video_name', t('You must provide a video name.'));
      }
      
      break;
    
    default:
      break;
  }
  
}


/**
 * Defines a custom submit callback 
 * It uploads a new video if necessary and retrives the video id
 * 
 * This callback is hooked into the node/add and node/<nid>/edit forms
 */
function videopublishing_upload_form_submit($form, &$form_state) {
  $v = $form_state['values']['videopublishing'];
  
  switch ($form_state['values']['videopublishing']['input_type']) {
    case 'video_id':
      $form_state['values']['videopublishing']['video_id'] = trim(check_plain($form_state['values']['videopublishing']['video_id']));
      break;
    
    case 'upload':
      if (isset($_FILES['files']['tmp_name']['videopublishing'])) {
        if ($file_tmp = $_FILES['files']['tmp_name']['videopublishing']) {
          // upload the video and get the video id 
          $video_name = trim(check_plain($form_state['values']['videopublishing']['video_name']));
          $form_state['values']['videopublishing']['video_id'] = videopublishing_upload_video($file_tmp, $video_name);
          // create stream and get the publish_id
          videopublishing_create_stream($form_state['values']['videopublishing']['video_id'], $form_state['values']['videopublishing']['format_id']);
          $form_state['values']['videopublishing']['publish_id'] = (string)videopublishing_publish_video($form_state['values']['videopublishing']['player_id'], $form_state['values']['videopublishing']['video_id'], $form_state['values']['videopublishing']['format_id']);
        }
      }
      break;
    
    default:
      break;
  }
  
}


/**
 * Defines a custom submit callback 
 * It saves the data in the database - {videopublishing} table
 * 
 * This function is called by nodeapi if $op is insert or update
 */
function videopublishing_upload_form_submit_db($nid, $vid, $vp) {
  
  $query = "
    DELETE FROM {videopublishing} 
    WHERE nid = %d AND vid = %d
  ";
  db_query($query, $nid, $vid);
  
  $query = "
    INSERT INTO {videopublishing} (nid, vid, video_id, publish_id, player_id, stream_id, delta)
    VALUES (%d, %d, '%s', '%s', '%s', %d, 0)
  ";
  db_query($query, $nid, $vid, $vp['video_id'], $vp['publish_id'], $vp['player_id'], $vp['format_id']);
  
}


/**
 * Defines a settings / admin form 
 * 
 * This form is rendered on the admin/settings/videopublishing page
 * used by videopublishing_settings_page()
 */
function videopublishing_settings_form($form_state) {
  $form = array();
  
  if (variable_get('videopublishing_api_user', '') && variable_get('videopublishing_api_user_key', '')) {
    $config_collapsed = TRUE;
    $default_player_options = videopublishing_settings_form_get_player_options();
  }
  else {
    $config_collapsed = FALSE;
    $default_player_options = array();
  }
  
  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('VideoPublishing.com Config'),
    '#collapsible' => TRUE,
    '#collapsed' => $config_collapsed,
    '#tree' => FALSE,
  );
  
  $form['config']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('API User'),
    '#default_value' => variable_get('videopublishing_api_user', ''),
  );
  
  $form['config']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('API User Key'),
    '#default_value' => variable_get('videopublishing_api_user_key', ''),
  );
  
  $form['default_player'] = array(
    '#type' => 'select',
    '#title' => t('Select default player'),
    '#default_value' => variable_get('videopublishing_default_player', NULL),
    '#options' => $default_player_options,
    '#description' => t('Select a default video player for this Drupal installation. You may select a different player for each node.'),
  );
  
  $form['content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Video Publishing content types'),
    '#default_value' => videopublishing_get_selected_content_types(),
    '#options' => videopublishing_get_content_type_options(),
    '#description' => t('VideoPublishing features will be enabled for the selected content types.'),
  );
  
  $form['video_id_widget'] = array(
    '#type' => 'select',
    '#title' => t('Video ID Widget'),
    '#default_value' => variable_get('videopublishing_video_id_widget', 'select'),
    '#options' => array('select' => t('Select list'), 'autocomplete' => t('AJAX Autocomplete')),
    '#description' => t('Choose the desired Video ID input type. Typically, the autocomplete widget is more usefull for selecting from a large number of videos.'),
  );
  
  $form['show_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show video titles'),
    '#default_value' => variable_get('videopublishing_show_video_title', FALSE),
    '#description' => t('Toggle the display of video titles. Note that this adds a bit of overhead because the this information is stored on the VideoPublishing server.'),
  );
  
  $form['show_in_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show videos in teasers'),
    '#default_value' => variable_get('videopublishing_show_in_teaser', FALSE),
    '#description' => t('Toggle the display of videos in node teasers.'),
  );
  
  $weights = range(-50, 50);
  $form['field_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#default_value' => variable_get('videopublishing_field_weight', 10),
    '#options' => array_combine($weights, $weights),
    '#description' => t('Select a weight for the VideoPublishing pseudo field.'),
  );
  
  $form['save_settings'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

/**
 * Defines a submit callback for videopublishing_settings_form
 * It saves the global Video Publishing Settings in the database
 */
function videopublishing_settings_form_submit($form, &$form_state) {
  variable_set('videopublishing_api_user', $form_state['values']['user']);
  variable_set('videopublishing_api_user_key', $form_state['values']['key']);
  
  variable_set('videopublishing_default_player', $form_state['values']['default_player']);
  variable_set('videopublishing_show_in_teaser', $form_state['values']['show_in_teaser']);
  variable_set('videopublishing_show_video_title', $form_state['values']['show_title']);
  variable_set('videopublishing_field_weight', $form_state['values']['field_weight']);
  variable_set('videopublishing_video_id_widget', $form_state['values']['video_id_widget']);
  
  $content_types = array();
  foreach ($form_state['values']['content_types'] as $type) {
    if ($type) {
      $content_types []= $type;
    }
  }
  variable_set('videopublishing_content_types', implode(',', $content_types));
  
  drupal_set_message(t('Your VideoPublishing configuration options have been saved.'));
}

