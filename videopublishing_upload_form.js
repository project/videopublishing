
function vpShowHideUpload(show){
  
  var video_upload_file = $('div#edit-videopublishing-video-name-wrapper');
  var video_player = $('div#edit-videopublishing-player-id-wrapper');
  var video_upload_name = $('div#edit-videopublishing-vp-video-upload-field-wrapper');
  var video_format = $('div#edit-videopublishing-format-id-wrapper');
  
  if (show) {
    $(video_upload_file).show(200);
    $(video_player).show(200);
    $(video_upload_name).show(200);
    $(video_format).show(200);
  } else {
    $(video_upload_file).hide(200);
    $(video_player).hide(200);
    $(video_upload_name).hide(200);
    $(video_format).hide(200);
  }
}


function vpShowHidePublishID(show) {
  var publish_id = $('div#vp-publish-id-wrapper');

  if (show) {
    $(publish_id).show(200);
  } else {
    $(publish_id).hide(200);
  }
}


function vpShowHideVideoID(show) {
  var video_id = $('div#edit-videopublishing-video-id-wrapper');

  if (show) {
    $(video_id).show(200);
  } else {
    $(video_id).hide(200);
  }
}


function vpSelectInputType(input_type) {
  
  switch(input_type) {
    case 'video_id':
      vpShowHideVideoID(true);
      vpShowHidePublishID(true);
      vpShowHideUpload(false);
    break;
    
    case 'upload':
      vpShowHideVideoID(false);
      vpShowHidePublishID(false);
      vpShowHideUpload(true);
    break;
  }
   
}

$(document).ready(function() {
  // select the video_id method by default
  
  var it = $('select#edit-videopublishing-input-type').val();
  vpSelectInputType(it);
  $("select#edit-videopublishing-input-type").change(function () {
    vpSelectInputType(this.value);
  });
    
  
});

