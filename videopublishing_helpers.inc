<?php 

/**
 * @file
 * Helper functions for the videopublishing module
 */

/**
 * Returns an array containing the user selected content types
 */
function videopublishing_get_selected_content_types() {
  $options = array();
  
  $ct = explode(',', variable_get('videopublishing_content_types', ''));
  foreach ($ct as $v) {
    $options[$v] = $v;
  }
  
  return $options;
}

/**
 * Returns an array containing all the defined 
 * VideoPublishign formats
 */
function videopublishing_get_format_options() {
  static $options;
  if (!empty($options)) {
    return $options;
  }
  
  $xml = videopublishing_get_formats();
  //header('Content-type: text/xml;');
  //echo $xml->asXML(); exit(0);
  foreach ($xml->format as $format) {
    $options[(int)$format->item] = (string)$format->title;
  }
  return $options;
}


/**
 * Returns an array containing all the VideoPublishing
 * video players available to the user
 */
function videopublishing_settings_form_get_player_options() {
  $options = array();
  
  $players = videopublishing_get_all_players();
  foreach ($players as $player) {
    if (isset($player->item)) {
      $options[(string)$player->item] = (string)$player->title .' ('. (string)$player->width .'x'. (string)$player->height .')';
    }
  }
  
  return $options;
}

/**
 * Returns an array containing all the content types
 */
function videopublishing_get_content_type_options() {
  $options = array();
  
  $query = 'SELECT type, name FROM {node_type};';
  $result = db_query($query);
  while ($row = db_fetch_array($result)) {
    $options[$row['type']] = t($row['name']);
  }
  
  return $options;
}

/**
 * Returns an array containing all the videos associated 
 * with the given node and version
 * 
 * Currently only one video per node is allowed
 */
function videopublishing_get_node_videos($nid, $vid = FALSE) {
  $videos = array();
  
  if (!$vid) {
    $vid = $nid;
  }
  
  $query = "
    SELECT video_id, publish_id, player_id, stream_id, delta 
    FROM {videopublishing}
    WHERE nid = %d AND vid = %d
    ORDER BY delta
  ";
  $result = db_query($query, $nid, $vid);
  
  while ($row = db_fetch_array($result)) {
    $videos []= $row;
  }
  
  return $videos;
}

/**
 * Deletes node data from the videopublishing table
 */
function videopublishing_delete_node_data($nid, $vid = FALSE) {
  $query = "DELETE FROM {videopublishing} WHERE nid = %d";
  if ($vid) {
    $query .= " AND vid = %d";
    db_query($query, $nid, $vid);
  }
  else {
    db_query($query, $nid);
  }
}

/**
 * @param string $video_id
 * Returns an associative array containing publish_ids
 */
function videopublishing_get_associated_publish_id_options($video_id, $required = FALSE, $extra = TRUE) {
  $pids = array();
  
  if (!$required) {
    $pids[''] = t(' - select a Publish ID - ');
  }
  
  $ea = videopublishing_get_element_associations($video_id);
  
  if ($ea) {
    foreach ($ea->association as $a) {
      if ($extra) {
        $element = (string)$a->element;
        $parts = explode('_', $element);
        $prefix = videopublishing_stream_id_label_mapper($parts[1]) .' - ';
      }
      else {
        $prefix = '';
      }
      $pids[(string)$a->item] = $prefix . t((string)$a->item);
    }
  }
  return $pids;
  
}

/**
 * Maps stream ids to their (hardcoded) labels
 */
function videopublishing_stream_id_label_mapper($stream_id) {
  $stream_id_map = array(
    1 => t('Standard'),
    2 => t('Enhanced'),
    3 => t('HD'),
  );
  
  return $stream_id_map[$stream_id];
}

/**
 * Returns an associative array
 * $a['video_id'] = 'video_title';
 */
function videopublishing_get_all_video_options($null_option = TRUE) {
  $vids = array();
  
  if ($null_option) {
    $vids[''] = t('Select a Video');
  }
  
  $all_vids_xml = videopublishing_get_all_videos();
  foreach ($all_vids_xml as $v) {
    $vids[(string)$v->item] = (string)$v->title;
  }

  return $vids;
}


/**
 * Check if the videopublishing library was added to the module.
 * It should be found in the VideoPublishing subdirectory
 */
function videopublishing_library_exists() {
  if (file_exists(API_BASE_DIR .'/library/Api')) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}