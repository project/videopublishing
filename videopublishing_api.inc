<?php

/**
 * @file
 * VideoPublishing API specific functions for the videopublishing module.
 */

/**
 * Config Settings
 */

define('API_BASE_DIR', drupal_get_path('module', 'videopublishing') .'/VideoPublishing');

define('API_ADDRESS', 'api.videopublishing.com');
define('API_USER', variable_get('videopublishing_api_user', ''));
define('API_USER_KEY', variable_get('videopublishing_api_user_key', ''));
define('CRLF', "\r\n");


/**
 * Get all defined players
 * 
 * Returns a SimpleXMLElement
 */
function videopublishing_get_all_players() {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::PLAYERS);
    
    $xml_string = $method->getAll();
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * Returns information about all the user's videos as a SimpleXMLElement
 */
function videopublishing_get_all_videos() {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::VIDEOS);
    
    $xml_string = $method->getAll();
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
}

/**
 * Get all streams for a given video 
 * 
 * Returns a SimpleXMLElement
 */
function videopublishing_get_all_streams($video_id) {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::STREAMS);
    
    if (!isset($video_id)) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined', 'error');
      }
    }
    
    $xml_string = $method->getAll();
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * DEPRECATED
 * Returns the embed code for the video as a SimpleXMLElement
 */
function videopublishing_get_stream_embed_code($video_id, $player_id, $format_id = 1) {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::STREAMS);
    
    if (!isset($video_id) || !isset($format_id) || !isset($player_id)) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined', 'error');
      }
    }
    
    $xml_string = $method->getEmbedCode($video_id, $format_id, $player_id);
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * Returns all available Video Publishing formats
 */
function videopublishing_get_formats() {
  _videopublishing_bootstrap();
  
  try {
  	$factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
  	$method = $factory->factory(Api_Methods_Factory::FORMATS);
  	
  	$xml_string = $method->getAll();
  	$xml = new SimpleXMLElement($xml_string);
  	return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}


/**
 * Uploads a new video and
 *
 * Returns the video_id as a string
 */
function videopublishing_upload_video($video_path, $video_name) {
  set_time_limit(600);
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::VIDEOS);
    
    if (!isset($video_path) || !isset($video_name)) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined for this video', 'error');
      }
    }
    
    $video_id = trim(strip_tags($method->upload($video_path, $video_name)));
    return $video_id;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * Returns extensive video information as a SimpleXMLElement
 */
function videopublishing_get_video_info($video_id) {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::VIDEOS);
    
    if (!isset($video_id)) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined for this video', 'error');
      }
    }
    
    $xml_string = $method->getInfo($video_id);
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * Create stream
 */
function videopublishing_create_stream($video_id, $format_id = 1) {
  _videopublishing_bootstrap();
  
  try {
  	$factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
  	$method = $factory->factory(Api_Methods_Factory::STREAMS);
  	
  	if (!isset($video_id) || !isset($format_id)) {
  	  if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined in videopublishing_create_stream()', 'error');
      }
  	}
  	
  	$xml_string = $method->setStream($video_id, $format_id);
  	$xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Stream API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Stream Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}


/**
 * Publish steam
 */
function videopublishing_publish_video($player_id, $video_id, $format_id = 1, $links = array()) {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::PUBLISH);
    
    //the $steam_id should have this format:
    //$stream_id = 'a597ccf196f7cc86e5793b8efee4a9b0_2';
    // the links array of strings format: ('<padding+weight>|<#seconds>|<action>')
    //'links'=>array('000001|30|pause')
    
    if (!isset($player_id) || !isset($format_id)) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined in videopublishing_publish_video()', 'error');
      }
    }
    
    $xml_string = $method->publish($player_id, $video_id .'_'. $format_id, 'stream', array('links' => $links));
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * Returns the HTML or HTML + JS embed code for the specified publish id
 */
function videopublishing_get_publish_embed_code($publish_id) {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::PUBLISH);
    
    if ( !isset($publish_id) ) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined in videopublishing_get_publish_id()', 'error');
      }
    }
    
    $xml_string = $method->getEmbed($publish_id);
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * Returns VideoPublishing associactions for the specified player or playlist
 */
function videopublishing_get_element_associations($element_id, $player_type = 'single') {
  _videopublishing_bootstrap();
  
  try {
    $factory = new Api_Methods_Factory(API_USER, API_USER_KEY, Api_Methods_Factory::XML_RESPONSE);
    $method = $factory->factory(Api_Methods_Factory::PUBLISH);
    
    if (!isset($element_id) || !isset($player_type)) {
      if (user_access('view videopublishing api error messages')) {
        drupal_set_message('Video Publishing: No data defined in videopublishing_get_element_associations()', 'error');
      }
    }
    
    $xml_string = $method->getElementAssociations($element_id, $player_type);
    $xml = new SimpleXMLElement($xml_string);
    return $xml;
  }
  
  catch (Api_Methods_Exception $api_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing API error: {$api_exception}", 'error');
    }
  }
  
  catch (Api_Http_RequestException $http_exception) {
    if (user_access('view videopublishing api error messages')) {
      drupal_set_message("Video Publishing Connection error: {$http_exception}", 'error');
    }
  }
  
  return FALSE;
}

/**
 * VideoPublishing bootstrap
 */
function _videopublishing_bootstrap() {
  spl_autoload_register('_videopublishing_autoload');
}

/**
 * Custom autoload
 */
function _videopublishing_autoload($_class_name) {
  $class = API_BASE_DIR . DIRECTORY_SEPARATOR .'library'. DIRECTORY_SEPARATOR . str_replace('_', DIRECTORY_SEPARATOR, $_class_name) .'.php';
  if (is_file($class)) {
    include_once $class;
  }
}